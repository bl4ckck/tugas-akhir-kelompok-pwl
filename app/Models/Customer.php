<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'address', 'phone', 'gender'];

    public $timestamps = false;

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
