<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $fillable = ['payment_type', 'total_amount', 'customer_id', 'user_id'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
