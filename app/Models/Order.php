<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $fillable = [
        'product_id',
        'user_id',
        'invoice_id',
        'quantity',
        'unit_price',
        'sub_total'
     ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
