<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $fillable = [
        'sales_id',
        'product_code',
        'quantity',
        'sub_total'
    ];

    public function product()
    {
           return $this->hasOne(Product::class);
        // return $this->hasOne(Product::class,'foreign_key');
    }
}
