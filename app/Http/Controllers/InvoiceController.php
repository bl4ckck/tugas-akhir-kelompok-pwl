<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class InvoiceController extends Controller
{
    public function index()
    {
        $invoice = Invoice::orderBy('id', 'desc')->get();
        $response = [
            'message' => 'List invoice order by invoice_id',
            'data' => $invoice
        ];
        return response()->json($response, Response::HTTP_OK);
    }

    public function show($id)
    {
        $invoice = Invoice::findOrFail($id);

        $response = [
            'message' => 'Detail of Invoice',
            'data' => $invoice
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    public function store(Request $request){
        $request->validate([
            'user_id' => 'required',
        ]);

        try {
            $invoice = Invoice::create($request->all());
            $response = [
                'message' => 'Invoice Created',
                'data'    => $invoice
            ];

            return response()->json($response, Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }
    
    public function update(Request $request, $id) {
        $invoice = Invoice::findOrFail($id);

        $request->validate([
            'total_amount' => 'required',
            'payment_type' => 'required'
        ]);

        try {
            $invoice->update($request->all());
            $response = [
                'message' => 'Invoice Updated',
                'data'    => $invoice
            ];

            return response()->json($response, 200);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }

    public function destroy($id)
    {
        $invoice = Invoice::findOrFail($id);

        try {
            $invoice->delete();
            $response = [
                'message' => 'Invoice Deleted'
            ];

            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }
}
