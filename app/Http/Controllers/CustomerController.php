<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;


//REFERENCE: https://stackoverflow.com/questions/56982227/call-to-undefined-method-stdclasstojson

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Customer::all();
        return view('customer.index', compact('data'));

        //PAGINATION
        // $data = Customer::latest()->paginate(5);

        // return view('customer.index', compact('data'))
        //     ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function getAll()
    {
        $data = Customer::all();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate requests
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'gender' => 'required',
        ]);

        Customer::create(
            $request->all()
        );

        return redirect()->route('dashboard')
                        ->with('success','Customer successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        $data = Customer::find($customer->id)->orders;

        return view('customer.show', compact('data', 'customer'));
    }

    public function testaja1()
    {
        $data = Customer::find(1)->orders;

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        // Validate requests
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'gender' => 'required',
        ]);

        $customer->update(
            $request->all()
        );

        return redirect()->route('dashboard')
                        ->with('success','Customer successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
        $customer->delete();

        return redirect()->route('dashboard')
                        ->with('success','Post deleted successfully');
    }
}
