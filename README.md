## Tugas Mata Kuliah Pemrograman Web Lanjut Kelas A

| Nama anggota | NIM |
| :--: | :--: |
| ALVIN NAUFAL | 185150401111008 | 
| DION RICKY SAPUTRA | 185150401111018 |
| JOVAN RICKTYAN SIMANJUNTAK | 185150400111020 |
| NADIR RUAYUANDA BAKRI | 185150400111036 |
| VIGO HERNANDO | 185150400111033 |

# Langkah-langkah Instalasi

### 1. Clone repository ini
```bash
git clone https://gitlab.com/bl4ckck/kelompok-laravel.git
```

### 2. Lakukan migrasi database
Jalankan perintah berikut ini untuk menerapkan struktur tabel database sekaligus mengisikan data kedalam database menggunakan seed.

> Mohon mengupdate detail database yang ada di .env
```bash
php artisan migrate:refresh --seed
```

### 3. Instalasi dependensi npm
Project kami membutuhkan npm untuk beberapa fungsionalitasnya seperti penggunaan framework css Tailwind dan pemrosesan postcss. Sehingga untuk menghasilkan tampilan yang menarik perlu diinstal beberapa dependensi npm. Masukkan perintah berikut ini:
```bash
npm install
```
Selanjutnya jalankan perintah berikut untuk melakukan compiling assets Vue Js, Alpine Js, dan Tailwind dengan laravel mix
```bash
npm run dev
```

### 4. Instalasi dependensi composer
Dengan memasukkan perintah ini, maka akan dilakukan instalasi dependensi composer
```bash
composer install
```

### 5. Menjalankan server
```bash
php artisan serve
```

**Username**: admin@admin.com<br/>
**Password**: 12345678
