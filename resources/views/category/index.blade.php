<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Product Category Dashboard') }}
        </h2>
    </x-slot>

    <div class="max-w-6xl mx-auto">
    <div class="flex flex-col items-center justify-center mx-5 md:mx-0">

    @if ($message = Session::get('success'))
        <div>
            <p>{{ $message }}</p>
        </div>
    @endif

        <a href="{{ route('category.create') }}" class="px-3 py-1 mt-3 text-sm font-bold text-purple-700 border-2 border-purple-600 rounded-lg">
            + | Data Product Category
    </a>

    <table class="w-full mt-10 table-fixed">
        <thead>
        <tr class="text-white bg-purple-500 shadow-lg">
            <th class="py-2">Name</th>
            <th class="py-2">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $key => $value)
        <tr>
            <td class="py-4 text-center border-b-2 border-gray-300">{{ $value->name }}</td>
            <td class="text-center border-b-2 border-gray-300">
                <form action="{{ route('category.destroy', $value->id) }}" method="POST">
                    <div class="flex justify-center space-x-4 font-bold">
                        <a href="{{ route('category.show', $value->id) }}" class="text-blue-500">Detail</a>

                        @csrf
                        @method('DELETE')
                        <button class="text-red-500">Delete</button>
                    </div>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>


    </div>
    </div>
</x-app-layout>
