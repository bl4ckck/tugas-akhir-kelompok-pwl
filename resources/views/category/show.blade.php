<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Category Detail') }}
        </h2>
    </x-slot>

    <div class="max-w-6xl mx-auto">
    <div class="flex flex-col items-center justify-center mx-5 md:mx-0">
        @if ($message = Session::get('success'))
            <div>
                <p>{{ $message }}</p>
            </div>
        @endif

        <div class="mt-10 text-4xl font-bold" style="color: #474747">
                Detail Category
        </div>
        <a href="{{ route('category.edit', $category->id) }}" class="px-3 py-1 mt-3 text-sm font-bold text-purple-700 border-2 border-purple-600 rounded-lg">
                Edit Data Category
        </a>

        <div class="flex flex-col">
            <div>{{ $category->name }}</div>
        </div>
    </div>
    </div>

</x-app-layout>
