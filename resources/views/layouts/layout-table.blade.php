<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link href="/css/app.css" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito';
            }
        </style>

        @yield('add-css')
    </head>
<body>
    <div class="max-w-6xl mx-auto">
        <div class="flex flex-col items-center justify-center mx-5 md:mx-0">
            @yield('content')
        </div>
    </div>
</body>
</html>
