<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Customer Detail') }}
        </h2>
    </x-slot>

    <div class="max-w-6xl mx-auto">
    <div class="flex flex-col items-center justify-center mx-5 md:mx-0">
        @if ($message = Session::get('success'))
            <div>
                <p>{{ $message }}</p>
            </div>
        @endif

        <div class="mt-10 text-4xl font-bold" style="color: #474747">
                Detail Customer
        </div>
        <a href="{{ route('customer.edit', $customer->id) }}" class="px-3 py-1 mt-3 text-sm font-bold text-purple-700 border-2 border-purple-600 rounded-lg">
                Edit Data Customer
        </a>

        <div class="flex flex-col text-center">
            @foreach ($data as $item)
            <div>{{ $item->order_date }}</div>
            <div><b>Total Price: </b>{{ $item->total_price }}</div>
            <div><b>Paid: </b>{{ $item->paid }}</div>
            <br>
            @endforeach
        </div>
    </div>
    </div>

</x-app-layout>
