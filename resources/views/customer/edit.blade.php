@extends('layouts.layout-table')

@section('title', 'Edit Customer')

@section('add-css')
    <style>
        .form-input,
        .form-textarea,
        .form-select,
        .form-multiselect {
            background-color: #edf2f7;
        }
    </style>
@endsection

@section('content')
    <div class="mt-10 text-4xl font-bold" style="color: #474747">
        Edit Customer
    </div>

    <!--Card-->
    <div class="w-3/4 p-8 mt-10 bg-white rounded shadow">
        <form action="{{ route('customer.update', $customer->id) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="mb-6 md:flex">
                <div class="md:w-1/3">
                    <label class="block pr-4 mb-3 font-bold text-gray-600 md:text-left md:mb-0" for="name-text">
                        Name
                    </label>
                </div>
                <div class="md:w-2/3">
                    <input class="block w-full form-input focus:bg-white" id="name-text" name="name" type="text" value="{{ $customer->name }}">
                </div>
            </div>

            <div class="mb-6 md:flex">
                <div class="md:w-1/3">
                    <label class="block pr-4 mb-3 font-bold text-gray-600 md:text-left md:mb-0" for="phone-text">
                        Phone Number
                    </label>
                </div>
                <div class="md:w-2/3">
                    <input name="phone" class="block w-full form-input focus:bg-white" id="phone-text" type="text" value="{{ $customer->phone }}">
                </div>
            </div>

            <div class="mb-6 md:flex">
                <div class="md:w-1/3">
                    <label class="block pr-4 mb-3 font-bold text-gray-600 md:text-left md:mb-0" for="gender">
                        Gender
                    </label>
                </div>
                <div class="md:w-2/3">
                    <select name="gender" class="block w-full form-select focus:bg-white" id="gender">
                        @if ($customer->gender === 'l')
                        <option value="l" selected>Male</option>
                        <option value="p">Female</option>
                        @else
                        <option value="l">Male</option>
                        <option value="p" selected>Female</option>
                        @endif
                    </select>

                </div>
            </div>

            <div class="mb-6 md:flex">
                <div class="md:w-1/3">
                    <label class="block pr-4 mb-3 font-bold text-gray-600 md:text-left md:mb-0" for="address-text">
                        Address
                    </label>
                </div>
                <div class="md:w-2/3">
                    <textarea name="address" class="block w-full form-textarea focus:bg-white" id="address-text" rows="8">{{ $customer->address }}</textarea>
                </div>
            </div>

            <div class="md:flex md:items-center">
                <div class="md:w-1/3"></div>
                <div class="md:w-2/3">
                    <input type="submit" class="px-4 py-2 font-bold text-white bg-purple-700 rounded shadow hover:bg-purple-500 focus:shadow-outline focus:outline-none">
                </div>
            </div>
        </form>

@endsection
