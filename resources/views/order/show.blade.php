<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Order Detail') }}
        </h2>
    </x-slot>

    <div class="max-w-6xl mx-auto">
    <div class="flex flex-col items-center justify-center mx-5 md:mx-0">
        @if ($message = Session::get('success'))
            <div>
                <p>{{ $message }}</p>
            </div>
        @endif

        <div class="mt-10 text-4xl font-bold" style="color: #474747">
                Detail Order
        </div>
        <a href="{{ route('order.edit', $order->id) }}" class="px-3 py-1 mt-3 text-sm font-bold text-purple-700 border-2 border-purple-600 rounded-lg">
                Edit Data Order
        </a>

        <div class="flex flex-col text-center">
            @foreach ($data as $item)
            <div>{{ $item->id }}</div>
            <div>{{ $item->order_date }}</div>
            <div>{{ $item->total_price }}</div>
            <div>{{ $item->paid }}</div>
            @endforeach
        </div>
    </div>
    </div>

</x-app-layout>
