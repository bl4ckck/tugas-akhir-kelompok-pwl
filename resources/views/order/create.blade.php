@extends('layouts.layout-table')

@section('title', 'Add Order')

@section('add-css')
    <style>
        .form-input,
        .form-textarea,
        .form-select,
        .form-multiselect {
            background-color: #edf2f7;
        }
    </style>
@endsection

@section('content')
    <div class="mt-10 text-4xl font-bold" style="color: #474747">
        Add Order
    </div>

    <!--Card-->
    <div class="w-3/4 p-8 mt-10 bg-white rounded shadow">
        <form action="{{ route('order.store') }}" method="POST">
            @csrf

            <div class="mb-6 md:flex">
                <div class="md:w-1/3">
                    <label class="block pr-4 mb-3 font-bold text-gray-600 md:text-left md:mb-0" for="id">
                        ID Customer
                    </label>
                </div>
                <div class="md:w-2/3">
                    <input class="block w-full form-input focus:bg-white" id="customer_id" name="customer_id" type="bigInteger" value="">
            </div>
            </div>

            <div class="mb-6 md:flex">
                <div class="md:w-1/3">
                    <label class="block pr-4 mb-3 font-bold text-gray-600 md:text-left md:mb-0" for="order_date">
                        Order Date
                    </label>
                </div>
                <div class="md:w-2/3">
                    <input class="block w-full form-input focus:bg-white" id="order_date" name="order_date" type="date" value="">
            </div>
            </div>

            <div class="mb-6 md:flex">
                <div class="md:w-1/3">
                    <label class="block pr-4 mb-3 font-bold text-gray-600 md:text-left md:mb-0" for="total_price">
                        Total Price
                    </label>
                </div>
                <div class="md:w-2/3">
                    <input class="block w-full form-input focus:bg-white" id="total_price" name="total_price" type="bigInteger" value="">
            </div>
            </div>

            <div class="mb-6 md:flex">
                <div class="md:w-1/3">
                    <label class="block pr-4 mb-3 font-bold text-gray-600 md:text-left md:mb-0" for="paid">
                        Paid
                    </label>
                </div>
                <div class="md:w-2/3">
                    <input class="block w-full form-input focus:bg-white" id="paid" name="paid" type="bigInteger" value="">
            </div>
        </div>

            <div class="md:flex md:items-center">
                <div class="md:w-1/3"></div>
                <div class="md:w-2/3">
                    <input type="submit" class="px-4 py-2 font-bold text-white bg-purple-700 rounded shadow hover:bg-purple-500 focus:shadow-outline focus:outline-none">
                </div>
            </div>
        </form>

@endsection
