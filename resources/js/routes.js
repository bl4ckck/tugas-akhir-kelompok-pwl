import Home from './components/Home.vue';
import CategoryIndex from './components/category/CategoryIndex.vue';
import CategoryCreate from './components/category/CategoryCreate.vue';
import CategoryShow from './components/category/CategoryShow.vue';
import CategoryEdit from './components/category/CategoryEdit.vue';

import ProductIndex from './components/product/ProductIndex.vue';
import ProductCreate from './components/product/ProductCreate.vue';
import ProductShow from './components/product/ProductShow.vue';
import ProductEdit from './components/product/ProductEdit.vue';

import OrderIndex from './components/order/OrderIndex.vue';
import OrderCreate from './components/order/OrderCreate.vue';
import OrderShow from './components/order/OrderShow.vue';
import OrderEdit from './components/order/OrderEdit.vue';

import OrderPage from './components/sales/OrderPage.vue';

export default [
    { name: 'dashboard', path: '/dashboard', component: Home },
    { name: 'orderPage', path: '/order/:id', component: OrderPage },

    // CATEGORY
    { name: 'categoryIndex', path: '/category', component: CategoryIndex },
    { name: 'categoryCreate', path: '/category/create', component: CategoryCreate },
    { name: 'categoryShow', path: '/category/:id', component: CategoryShow },
    { name: 'categoryEdit', path: '/category/edit/:id', component: CategoryEdit },

    // PRODUCT
    { name: 'productIndex', path: '/product', component: ProductIndex },
    { name: 'productCreate', path: '/product/create', component: ProductCreate },
    { name: 'productShow', path: '/product/:id', component: ProductShow },
    { name: 'productEdit', path: '/product/edit/:id', component: ProductEdit },

    // ORDER
    { name: 'orderIndex', path: '/order', component: OrderIndex },        
    { name: 'orderCreate', path: '/order/create', component: OrderCreate },
    { name: 'orderShow', path: '/order/:id', component: OrderShow },
    { name: 'orderEdit', path: '/order/edit/:id', component: OrderEdit },
];
