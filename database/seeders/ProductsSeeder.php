<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'product_code' => Str::random(5),
            'product_name' => 'Indomilk Full Cream PTH.BOX',
            'category_id' => 2,
            'price' => 120000.00
        ]);

        DB::table('products')->insert([
            'product_code' => Str::random(5),
            'product_name' => 'Indomie Goreng ORI.BOX',
            'category_id' => 1,
            'price' => 50400.00
        ]);

        DB::table('products')->insert([
            'product_code' => Str::random(5),
            'product_name' => 'Beras Bramo Super BRM.25',
            'category_id' => 4,
            'price' => 280000.00
        ]);

        DB::table('products')->insert([
            'product_code' => Str::random(5),
            'product_name' => 'Beras Raja Lele RLE.25',
            'category_id' => 4,
            'price' => 250000.00
        ]);

        DB::table('products')->insert([
            'product_code' => Str::random(5),
            'product_name' => 'Terigu Segitiga Biru SBR.1',
            'category_id' => 4,
            'price' => 12000.00
        ]);

        DB::table('products')->insert([
            'product_code' => Str::random(5),
            'product_name' => 'GPU Besar LRG.350',
            'category_id' => 3,
            'price' => 11000.00
        ]);

        DB::table('products')->insert([
            'product_code' => Str::random(5),
            'product_name' => 'Kayu Putih Kecil SML.75',
            'category_id' => 3,
            'price' => 19000.00
        ]);
    }
}
