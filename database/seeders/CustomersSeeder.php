<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'name' => 'Toko Bintang Putra Jaya',
            'address' => 'Jl. Merdeka No. 50, Malang',
            'phone' => '08192849912',
            'gender' => 'l',
        ]);

        DB::table('customers')->insert([
            'name' => 'UD. Tiga Putri',
            'address' => 'Jl. Kenangan No. 3, Surabaya',
            'phone' => '08583911234',
            'gender' => 'p',
        ]);

        DB::table('customers')->insert([
            'name' => 'Toko Citra',
            'address' => 'Jl. Masjid, Klojen, Malang',
            'phone' => '08321148192',
            'gender' => 'l',
        ]);
    }
}
