<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            'product_id' => 1,
            'unit_price' => 120000,
            'quantity' => 10,
            'sub_total' => 1200000
        ]);

        DB::table('orders')->insert([
            'product_id' => 2,
            'unit_price' => 50400,
            'quantity' => 10,
            'sub_total' => 504000
        ]);

    }
}
