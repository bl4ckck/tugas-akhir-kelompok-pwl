<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sales')->insert([
            'sales_id' => 1,
            'product_code' =>Str::random(20),
            'quantity' => random_int(1,10000),
            'sub_total' => random_int(1, 10000000),
        ]);
    }
}
