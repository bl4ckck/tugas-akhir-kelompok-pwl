<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ProductController;

use App\Http\Controllers\OrderController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\InvoiceController;

use Illuminate\Support\Facades\Auth;

use Symfony\Component\HttpFoundation\Response;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('testapi1', [CustomerController::class, 'testaja1']);

Route::resource('/product', ProductController::class)->except('create', 'edit');

Route::resource('/sale', SaleController::class)->except('create', 'edit');


// Category Route
require __DIR__.'/api/CategoryRoute.php';
require __DIR__.'/api/SaleRoute.php';

// Hanya diakses melalui API, tidak ada create dan edit
// Route::resource('/invoice', InvoiceController::class)->except('create', 'edit');

require __DIR__.'/api/InvoiceRoute.php';
require __DIR__.'/api/CustomerRoute.php';
require __DIR__.'/api/OrderRoute.php';
