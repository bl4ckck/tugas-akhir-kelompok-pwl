<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| API Product Category
|--------------------------------------------------------------------------
*/
// Get All Data Categories
Route::get('categories', [CategoryController::class, 'getAll']);

// Get Data Categories
Route::get('categories/{id}', [CategoryController::class, 'getOne']);

// Insert Data Categories
Route::post('categories', [CategoryController::class, 'store']);

// Update Data Categories
Route::put('categories/{id}/update', [CategoryController::class, 'update']);

// Delete Data Categories
Route::delete('categories/{id}', [CategoryController::class, 'destroy']);
