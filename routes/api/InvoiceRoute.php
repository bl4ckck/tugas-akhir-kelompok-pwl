<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InvoiceController;

Route::get('invoice', [InvoiceController::class, 'index']);

Route::get('invoice/{id}', [InvoiceController::class, 'show']);

Route::post('invoice', [InvoiceController::class, 'store']);

Route::put('invoice/{id}', [InvoiceController::class, 'update']);

Route::delete('invoice/{id}', [InvoiceController::class, 'destroy']);
