<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SaleController;

/*
|--------------------------------------------------------------------------
| API Sales
|--------------------------------------------------------------------------
*/
// Get All Data Sales
Route::get('sales', [SaleController::class, 'getAll']);

// Get Data Sales
Route::get('sales/{id}', [SaleController::class, 'getOne']);

// Insert Data Sales
Route::post('sales', [SaleController::class, 'store']);

// Update Data Sales
Route::put('sales/{id}/update', [SaleController::class, 'update']);

// Delete Data Sales
Route::delete('sales/{id}', [SaleController::class, 'destroy']);
